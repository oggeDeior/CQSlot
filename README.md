# CQSLOT

#### 介绍
    
```
    这是一个很棒的跨平台线程安全的C++信号槽，使用方法如同Qt里的信号槽，甚至更简单。
全部代码量只有２百多行．核心代码只有几十行。依赖c++11 可以在任何平台运行。
```





具体使用可以参考main.cpp

```
int main()
{
	// 定义一个独立的线程对象，后面可能用得上
	CQSLOT::CQThread th;
	// 定义完就启动
	th.start();

	// 测试类1，我们让它跑在独立线程上
	Test1 test1;
	// 这里意思是我希望我的目标槽函数在独立线程中跑，而不是在主线程中执行
	test1.moveToThread(&th);

	// 测试类2，我们让它直接在主线程上跑
	Test2 test2;

	// 定义一个线程信号类，其实不定义也可以，为了演示而已，直接用CQ_EMIT发射信号也可
	ThreadTest tt;
	// 把一个信号（可以理解为触发一个函数，既可以是C函数，也可以是类成员函数）绑定到目标槽函数，可以绑n个，
    // 它们是“m <---> n”的对应关系，非常灵活
	CQ_CONNECT(tt.m_addSignal, &test1, &Test1::add);
	CQ_CONNECT(tt.m_addSignal, &test2, &Test2::reduce);
	// 启动线程，这个线程内部会立即发射信号，信号会立即触发上面connect连接的槽函数
	tt.start();

	// 演示2，定时器类，定时器定时结束就会发射信号
	CQSLOT::CQTimer timer;
	// 把定时器“定时结束”的信号绑定后面的函数，跟上面一样，“m <---> n”的对应关系
	CQ_CONNECT(timer.timeout, &test1, &Test1::timeOut);
	CQ_CONNECT(timer.timeout, &test2, &Test2::timeOut);
	// 定时器开始计时
	timer.start(5000);

	// 开启事件循环
	return CQ_EXEC();
}
```

快来试试吧

