#include "./include/CQ_looper.h"

DEFINE_NAMESPACE_CQ_BEGIN

std::map<std::thread::id, CQLooper*> CQLooper::m_loopers;
std::mutex CQLooper::m_mutex;

DEFINE_NAMESPACE_CQ_END